class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find_by(id: params[:id])
    @related_products = Spree::Taxon.find_by(id: @product.taxon_ids.first).products.where.not(id: @product.id)
  end
end
