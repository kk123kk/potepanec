class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxon.roots
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.products
  end
end
